import React from 'react'
import { Paper, Typography } from '@material-ui/core';
import "../sass/components/addButton.scss";
import AddIcon from '@material-ui/icons/Add';


export default ( props ) => {
  return (
    <div className="container">
      <section className="addbutton">
        <Typography gutterBottom variant="subheading" className="addbutton__header">To do</Typography>
        <Paper 
          className="addbutton__paper" 
          square={true}
          onClick={() => props.onAddItem()}
        >
          <AddIcon className="addbutton__button"/>
        </Paper>
      </section>
    </div>
  )
}
