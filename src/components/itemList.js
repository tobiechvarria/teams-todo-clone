import React, { Component } from "react";
import {
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Typography
} from "@material-ui/core/";
import "../sass/components/item-list.scss";
import Calendar from "@material-ui/icons/CalendarToday";
import Comment from "@material-ui/icons/Comment";
import Attachment from "@material-ui/icons/Attachment";

export default class ItemList extends Component {
  state = {
    items: [
      {
        title: "item",
        startDate: "04/24",
        dueDate: "",
        complete: false,
        assignTo: []
      }
    ]
  };
  render() {
    return (
      <div className="container">
        {this.state.items.map((item, index) => (
          <Card key={index} className="card" square={true}>
            {/* <CardHeader
                                key={index}
                                title={item.title}>
                             /></CardHeader> */}
            <CardContent className="card__content">
            {item.title}
              <div className="card__content__top">                
                <div className="card__content__date">
                  {item.startDate}
                  <Calendar />
                </div>
                <Comment />
                <Attachment />
              </div>
              <hr/>
            </CardContent>
          </Card>
        ))}
      </div>
    );
  }
}
