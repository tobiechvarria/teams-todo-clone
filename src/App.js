import React, { Component, Fragment } from 'react';
import AddButton from './components/addButton'
import ItemList from './components/itemList';
import './sass/main.scss';
class App extends Component {

  addItem = () =>{
    console.log("addItem");
  }
  render() {
    return (
      <Fragment>
        <AddButton onAddItem={this.addItem}/>
        <ItemList></ItemList>
      </Fragment>
    );
  }
}

export default App;
